Feature: Test poc users
  Test users features

  Scenario: We should be able to insert an user
    Given a valid user email "geoge@clem.go" and a valid user name "GeogeClem"
    When calling "POST" on "v1/users"
    Then user should be created with an id


  Scenario: We should not be able to insert an incorrect user
    Given a valid user email "geoge@clem.go" and an invalid name ""
    When calling "POST" on "v1/users"
    Then user should not be created
    And an error "name" missing should be found

    Given an invalid user email "" and a valid user name "GeogeClem"
    When calling "POST" on "v1/users"
    Then user should not be created
    And an error "email" missing should be found
