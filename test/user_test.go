package test_test

import (
	"encoding/json"
	"fmt"
	"github.com/DATA-DOG/godog"
	"github.com/matryer/runner"
	"net/http"
	"strings"
	"gitlab.com/ty-go-poc/user/proto"
)

var (
	email           string
	name            string
	services        *runner.Task
	client          *http.Client
	requestResponse *http.Response
	responseUser    proto.User
)

func userData(pEmail string, pName string) error {
	email = pEmail
	name = pName
	return nil
}

func insertingUser(method string, endpoint string) error {
	switch method {
	case "POST":
		resp, err := client.Post(
			"http://localhost:8080/"+endpoint, "application/json",
			strings.NewReader(fmt.Sprintf("{\"name\":\"%s\",\"email\":\"%s\"}", name, email)),
		)
		if err != nil {
			return fmt.Errorf("should not raise error. Error: %s", err.Error())
		}
		requestResponse = resp
		decoder := json.NewDecoder(requestResponse.Body)
		_ = decoder.Decode(&responseUser)
	default:
		return fmt.Errorf("unexpected call")
	}
	return nil
}

func userShouldBeCreatedWithAnId() error {
	if requestResponse == nil {
		return fmt.Errorf("post should return an object")
	}
	if requestResponse.StatusCode != http.StatusCreated {
		return fmt.Errorf("expected status 201. Got: %d", requestResponse.StatusCode)
	}

	if responseUser.Id <= 0 {
		return fmt.Errorf("user ID should be define. got: %d", responseUser.Id)
	}

	return nil
}

func userNotCreated() error {
	if requestResponse.StatusCode != http.StatusBadRequest {
		return fmt.Errorf("expected status %d. Got: %d", http.StatusBadRequest, requestResponse.StatusCode)
	}
	return nil
}

func gotError(errorName string) error {
	return nil
}

func UserFeatureContext(s *godog.Suite) {
	s.Step(`^a valid user email "([^"]*)" and a valid user name "([^"]*)"$`, userData)
	s.Step(`^a valid user email "([^"]*)" and an invalid name "([^"]*)"$`, userData)
	s.Step(`^an invalid user email "([^"]*)" and a valid user name "([^"]*)"$`, userData)
	s.Step(`^calling "([^"]*)" on "([^"]*)"`, insertingUser)
	s.Step(`^user should be created with an id$$`, userShouldBeCreatedWithAnId)
	s.Step(`^user should not be created$`, userNotCreated)
	s.Step(`^an error "([^"]*)" missing should be found$`, gotError)

	s.BeforeSuite(func() {
		client = &http.Client{}
	})

	s.BeforeScenario(func(interface{}) {
		name = ""
		email = ""
	})

}
