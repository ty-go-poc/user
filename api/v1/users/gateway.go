package users

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/ty-go-poc/user/proto"
	"net/http"
	"strconv"
)

var (
	log     *logrus.Logger
	service proto.UserService
)

func NewGateWay(router *gin.RouterGroup, logger *logrus.Logger) {
	log = logger
	rest := "rest"
	service = proto.NewClient(&rest, nil, logger)

	userGW := router.Group("/users")
	userGW.GET("", getAll)
	userGW.POST("", newUser)
	userGW.POST("login", loginUser)
	// Just to make thinks readable
	{
		userGWId := userGW.Group("/:id")
		userGWId.GET("", getSingle)
		// userGWId.PUT("", update)
		// userGWId.PATCH("", update)
	}
}

func getAll(c *gin.Context) {
	res, err := service.ReadAll(c, &proto.ReadAllRequest{Api: "v1"})
	if err != nil {
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}
	c.JSON(http.StatusOK, res)
}

func getSingle(c *gin.Context) {
	id, argErr := strconv.ParseInt(c.Param("id"), 10, 64)
	if argErr != nil {
		c.JSON(http.StatusBadRequest, "ID is not an integer")
		return
	}
	res, err := service.Read(c, &proto.ReadRequest{Api: "v1", Id: id})
	if err != nil {
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}
	c.JSON(http.StatusOK, res)
}

func newUser(c *gin.Context) {
	var userData proto.User
	if c.ContentType() == "multipart/form-data" {
		if err := c.Bind(&userData); err != nil {
			log.Info(err)
			c.JSON(http.StatusBadRequest, err.Error())
			return
		}
	} else {
		if err := c.BindJSON(&userData); err != nil {
			log.Info(err)
			c.JSON(http.StatusBadRequest, err.Error())
			return
		}
	}
	res, err := service.Create(c, &proto.CreateRequest{Api: "v1", User: &userData})
	if err != nil {
		log.Error(err)
		c.JSON(http.StatusBadRequest, err)
		return
	}
	c.JSON(http.StatusCreated, res)
}

func loginUser(c *gin.Context) {
	var loginData struct {
		ID       string
		Password string
	}
	if c.ContentType() == "multipart/form-data" {
		if err := c.Bind(&loginData); err != nil {
			log.Info(err)
			c.JSON(http.StatusBadRequest, err.Error())
			return
		}
	} else {
		if err := c.BindJSON(&loginData); err != nil {
			log.Info(err)
			c.JSON(http.StatusBadRequest, err.Error())
			return
		}
	}
	res, err := service.Login(c, &proto.LoginRequest{Api: "v1", Id: loginData.ID, Password: loginData.Password})
	if err != nil {
		log.Error(err)
		c.JSON(http.StatusBadRequest, err)
		return
	}
	c.JSON(http.StatusCreated, res)
}
