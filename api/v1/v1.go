package v1

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"user/api/v1/users"
)

func InitEndpoints(router *gin.RouterGroup, logger *logrus.Logger) {
	users.NewGateWay(router, logger)
}
