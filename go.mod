module gitlab.com/ty-go-poc/user

go 1.13

require (
	github.com/DATA-DOG/go-sqlmock v1.4.0
	github.com/DATA-DOG/godog v0.7.13
	github.com/appleboy/gin-revision-middleware v0.0.0-20160722161421-cc548e45b51a
	github.com/gin-contrib/cors v1.3.0
	github.com/gin-gonic/gin v1.5.0
	github.com/golang/protobuf v1.3.2
	github.com/jmoiron/sqlx v1.2.0
	github.com/matryer/runner v0.0.0-20190427160343-b472a46105b1
	github.com/micro/cli v0.2.0
	github.com/micro/go-micro v1.18.0
	github.com/sirupsen/logrus v1.4.2
	github.com/smartystreets/goconvey v1.6.4
	gitlab.com/ty-go-poc/core v1.0.7
)
