package user_test

import (
	. "github.com/smartystreets/goconvey/convey"
	"testing"
	"gitlab.com/ty-go-poc/user/module/user"
)

func TestUserModel(t *testing.T) {
	Convey("IsValid", t, func() {
		Convey("Should error when name is not valid", func() {
			usr := user.User{
				Id:    123,
				Name:  "",
				Email: "Test",
			}
			err := usr.IsValid()
			So(err, ShouldNotBeNil)
			So(err.From, ShouldEqual, "User.IsValid")
			So(err.Element, ShouldEqual, "name")
		})

		Convey("Should error when email is not valid", func() {
			usr := user.User{
				Id:   123,
				Name: "Some name",
			}
			err := usr.IsValid()
			So(err, ShouldNotBeNil)
			So(err.From, ShouldEqual, "User.IsValid")
			So(err.Element, ShouldEqual, "email")
		})

		Convey("Should return nil when user is valid", func() {

			usr := user.User{
				Id:    123,
				Name:  "Some name",
				Email: "Some email",
			}
			err := usr.IsValid() // Failing test to check lib
			So(err, ShouldBeNil)
		})
	})
}
