package user_test

import (
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
	"gitlab.com/ty-go-poc/user/module/user"
)

func TestUserDao(t *testing.T) {
	// Init testing frameworks

	// Init DB Mock
	sqlDb, mock, _ := sqlmock.New()
	db := sqlx.NewDb(sqlDb, "sqlmock")

	Convey("GetAll", t, func() {
		Convey("Should get all users from DB in a list", func() {
			rows := sqlmock.NewRows([]string{"id", "name", "email"}).
				AddRow(1, "someone", "some@one.fr").
				AddRow(2, "king", "kong@king.fr").
				AddRow(3, "Platon", "platon@history.gc")
			// ExpectQuery is a regex declare as string
			mock.ExpectQuery("SELECT [*] from users").WillReturnRows(rows)
			res := user.GetAll(db)
			So(res, ShouldNotBeZeroValue)
			for _, u := range *res {
				switch u.Id {
				case 1:
					So(u.Email, ShouldEqual, "some@one.fr")
					So(u.Name, ShouldEqual, "someone")
				case 2:
					So(u.Email, ShouldEqual, "kong@king.fr")
					So(u.Name, ShouldEqual, "king")
				case 3:
					So(u.Email, ShouldEqual, "platon@history.gc")
					So(u.Name, ShouldEqual, "Platon")
				default:
					So(u, ShouldBeNil)
				}
			}
		})
		Convey("Should return empty list on errors", func() {
			// ExpectQuery is a regex declare as string
			mock.ExpectQuery("SELECT [*] from users").WillReturnError(sqlmock.ErrCancelled)
			res := user.GetAll(db)
			So(res, ShouldNotBeNil)
			So(res, ShouldResemble, &user.List{})
		})
	})

	Convey("Insert", t, func() {
		// Init testing variables
		var ID int64
		usrToInsert := user.User{
			Name:  "someone",
			Email: "troll",
		}

		Convey("Should insert user and set correct last inserted ID to item", func() { // ExpectQuery is a regex declare as string
			mock.ExpectQuery("INSERT INTO users").WillReturnRows(
				sqlmock.NewRows([]string{"id"}).AddRow("5"),
			)
			err := user.Insert(&usrToInsert, db)
			ID = 5
			So(err, ShouldBeNil)
			So(usrToInsert.Id, ShouldEqual, ID)

			// ExpectQuery is a regex declare as string
			mock.ExpectQuery("INSERT INTO users").WillReturnRows(
				sqlmock.NewRows([]string{"id"}).AddRow("7"),
			)
			err = user.Insert(&usrToInsert, db)
			ID = 7
			So(err, ShouldBeNil)
			So(usrToInsert.Id, ShouldEqual, ID)
		})
	})
}
