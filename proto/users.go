package proto

import (
	"context"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
	"github.com/sirupsen/logrus"
	"gitlab.com/ty-go-poc/core"
	"gitlab.com/ty-go-poc/user/module/user"
)
const(
	// DefaultClientName Export default naming for client name
	DefaultClientName = "cli"
	// DefaultServerName Export default naming for server services name
	DefaultServerName = "todo"
)


type UserHandler struct {
	dbStore *core.DBStore
}

//  ---------- INITIALIZER ----------------
// NewHandler initialize the handler for TaskHandler processing
func NewHandler(connection *core.DbConfig, logger *logrus.Logger) UserHandler { // nolint:golint
	dbStore := core.NewDBStore(
		connection.User, connection.Database,
		connection.Password, connection.Host,
		connection.Port, logger,
	)
	return UserHandler{dbStore: dbStore}
}

// NewClient init a task client
func NewClient(name *string, serverName *string, logger *logrus.Logger) UserService {
	if name == nil {
		cname := DefaultClientName
		name = &cname
	}
	if serverName == nil {
		sname := DefaultServerName
		serverName = &sname
	}
	service := micro.NewService(
		micro.Name("task."+*name),
		micro.WrapHandler(func(fn server.HandlerFunc) server.HandlerFunc { return core.EnforceVersion(fn, logger) }),
	)
	service.Init()
	return NewUserService(*serverName, service.Client())
}

//  ---------- FEATURE ----------------
func (u UserHandler) Create(ctx context.Context, req *CreateRequest, rsp *CreateResponse) error { // nolint:golint
	uM := req.User.toModel()
	err := user.Insert(&uM, u.dbStore.Db)
	if err != nil {
		return err
	}
	rsp.Id = uM.Id
	rsp.Api = "v1"
	return nil
}

func (u UserHandler) Update(ctx context.Context, req *UpdateRequest, rsp *UpdateResponse) error {
	uM := req.User.toModel()

	// err := user.Update(&uM, u.dbStore.Db)
	//	// if err != nil {
	//	// 	return err
	//	// }

	rsp.User = modelToProto(&uM)
	rsp.Api = "v1"
	return nil
}

func (u UserHandler) Read(ctx context.Context, req *ReadRequest, rsp *ReadResponse) error {
	rsp.User = modelToProto(user.GetOne(req.Id, u.dbStore.Db))
	rsp.Api = "v1"
	return nil
}

func (u UserHandler) ReadAll(ctx context.Context, req *ReadAllRequest, rsp *ReadAllResponse) error {
	rsp.Users = modelToProtoList(user.GetAll(u.dbStore.Db))
	rsp.Api = "v1"
	return nil
}

func (u UserHandler) Login(ctx context.Context, req *LoginRequest, rsp *LoginResponse) error {
	usr, err := user.Login(req.Id, req.Password, u.dbStore.Db)
	rsp.Api = "v1"
	if err == nil {
		rsp.User = modelToProto(usr)
	}
	return err
}

// ---------- CONVERTERS ----------------
func (m *User) toModel() user.User {
	return user.User{
		Id:       m.Id,
		Name:     m.Name,
		Email:    m.Email,
		Password: m.Password,
	}
}

func modelToProto(u *user.User) *User {
	return &User{
		Id:       u.Id,
		Name:     u.Name,
		Email:    u.Email,
		Password: u.Password,
	}
}

func modelToProtoList(ul *user.List) []*User {
	var res []*User
	for _, u := range *ul {
		proto := modelToProto(u)
		res = append(res, proto)
	}
	return res
}
