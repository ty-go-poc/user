package client

import (
	"context"
	"github.com/sirupsen/logrus"
	"gitlab.com/ty-go-poc/user/proto"
)

var (
	log = logrus.New()
)

type UserClient struct {
	cli proto.UserService
	serverName string
	cliName string
	api string
}

type UserResponse proto.User

func NewClient(serverName, cliName, api string) *UserClient {
	usrCli := UserClient{
		cli: proto.NewClient(&cliName, &serverName, log),
		serverName: serverName,
		cliName: cliName,
		api: api,
	}
	return &usrCli
}

func (uc *UserClient) GetFromID(ID int64) (*UserResponse, error) {
	resp, err := uc.cli.Read(context.Background(), &proto.ReadRequest{Id:ID, Api: uc.api})
	if err != nil {
		return nil, err
	}
	return (*UserResponse)(resp.User), nil
}
