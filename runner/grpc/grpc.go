package main

import (
	"fmt"
	"github.com/micro/cli"
	micro "github.com/micro/go-micro"
	"github.com/sirupsen/logrus"
	"gitlab.com/ty-go-poc/core"
	"os"
	"user/api/v1/users"
)

var (
	dbConf        core.DbConfig
	log           *logrus.Logger
	serviceConfig core.ServiceConfig
	newLogger     func() *logrus.Logger
)

func getConfig(filePath string) {
	var ok bool
	// var err error
	// filePath, err = filepath.Abs(filePath)
	// if err != nil {
	// 	fmt.Printf("Could not get file: %s", filePath)
	// 	os.Exit(1)
	// }
	ok, dbConf, serviceConfig, newLogger = core.GetConfig(&filePath)
	if !ok {
		fmt.Printf("Could not retrieve config: %s", filePath)
		os.Exit(1)
	}
	log = newLogger()
}

func main() {
	// Create a new service. Optionally include some options here.
	configPath := core.GetEnvDefault("CONFIG_FILE_PATH", "config.yml")
	getConfig(configPath)
	service := micro.NewService(
		micro.Name(serviceConfig.ServiceName),
		micro.Version("v1"),
	)

	// Init will parse the command line flags.
	service.Init(micro.Action(func(c *cli.Context) {}))

	// Register handler
	if err := users.RegisterUserServiceHandler(service.Server(), users.NewHandler(&dbConf, log)); err != nil {
		log.Error(err)
	}
	// Run the server
	if err := service.Run(); err != nil {
		fmt.Println(err)
	}
}
