package main

import (
	"fmt"
	web "github.com/micro/go-micro/web"
	"github.com/sirupsen/logrus"
	"gitlab.com/ty-go-poc/core"
	"gitlab.com/ty-go-poc/user/api"
)

var (
	serviceConfig core.ServiceConfig
	log           *logrus.Logger
	newLogger     func() *logrus.Logger
)

func getConfig(filepath string) {
	var ok bool
	ok, _, serviceConfig, newLogger = core.GetConfig(&filepath)
	if !ok {
		fmt.Printf("could not get logs")
	}
	log = newLogger()
}
func main() {
	configPath := core.GetEnvDefault("CONFIG_FILE_PATH", "config.yml")
	getConfig(configPath)
	// Create a new service. Optionally include some options here.
	service := web.NewService(
		web.Name(fmt.Sprintf("%s.rest", serviceConfig.ServiceName)),
		web.Handler(api.GetAPIRouter(log)),
		web.Address(fmt.Sprintf("%s:%d", serviceConfig.Hostname, serviceConfig.RESTPort)),
		web.Version("v1"),
	)

	if err := service.Init(); err != nil {
		log.Fatal(err)
	}
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
